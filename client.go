package main

import (
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync/atomic"
	"time"
)

func sendPost(u *url.URL, b io.Reader) ([]byte, error) {
	var count uint64
	var err error
	var body []byte

	for count < 3 {
		body, err = try(u, b)
		if err == nil {
			return body, nil
		}
		atomic.AddUint64(&count, 1)
	}
	return nil, errors.WithStack(err)
}

func try(u *url.URL, b io.Reader) ([]byte, error) {
	req, err := http.NewRequest("POST", u.String(), b)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{
		Timeout: 2 * time.Second,
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return body, nil
}
