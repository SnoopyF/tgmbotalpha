package main

type sendMessageMethod struct {
	ChatId              int         `json:"chat_id"`
	Text                string      `json:"text"`
	ParseMode           string      `json:"parse_mode,omitempty"`
	DisableNotification bool        `json:"disable_notification,omitempty"`
	ReplyToMessageId    int         `json:"reply_to_message_id,omitempty"`
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`
}


type getUpdatesMethod struct {
	Offset int `json:"offset,omitempty"`
	Limit  int `json:"limit,omitempty"`
}

type forwardMessageMethod struct {
	ChatId              int  `json:"chat_id"`
	FromChatId          int  `json:"from_chat_id"`
	DisableNotification bool `json:"disable_notification,omitempty"`
	MessageId           int  `json:"message_id"`
}

type sendPhotoMethod struct {
	ChatId              int         `json:"chat_id"`
	Photo               string      `json:"photo"`
	Caption             string      `json:"caption"`
	DisableNotification bool        `json:"disable_notification,omitempty"`
	ReplyToMessageId    int         `json:"reply_to_message_id,omitempty"`
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`
}

type sendAudioMethod struct {
	ChatId              int         `json:"chat_id"`
	Audio               string      `json:"audio"`
	Caption             string      `json:"caption"`
	ParseMode           string      `json:"parse_mode,omitempty"`
	Duration            int         `json:"duration,omitempty"`
	Performer           string      `json:"performer,omitempty"`
	Title               string      `json:"title,omitempty"`
	Thumb               string      `json:"thumb,omitempty"`
	DisableNotification bool        `json:"disable_notification,omitempty"`
	ReplyToMessageId    int         `json:"reply_to_message_id,omitempty"`
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`
}

type sendDocumentMethod struct {
	ChatId              int         `json:"chat_id"`
	Document            string      `json:"document"`
	Thumb               string      `json:"thumb,omitempty"`
	Caption             string      `json:"caption"`
	ParseMode           string      `json:"parse_mode,omitempty"`
	DisableNotification bool        `json:"disable_notification,omitempty"`
	ReplyToMessageId    int         `json:"reply_to_message_id,omitempty"`
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`
}

type sendVideoMethod struct {
	ChatId              int         `json:"chat_id"`
	Video               string      `json:"video"`
	Duration            int         `json:"duration,omitempty"`
	Width               int         `json:"width,omitempty"`
	Height              int         `json:"height,omitempty"`
	Thumb               string      `json:"thumb,omitempty"`
	Caption             string      `json:"caption,omitempty"`
	ParseMode           string      `json:"parse_mode,omitempty"`
	SupportsStreaming   string      `json:"supports_streaming,omitempty"`
	DisableNotification bool        `json:"disable_notification,omitempty"`
	ReplyToMessageId    int         `json:"reply_to_message_id,omitempty"`
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`
}

type sendAnimationMethod struct {
	ChatId              int         `json:"chat_id"`
	Animation           string      `json:"animation"`
	Duration            int         `json:"duration,omitempty"`
	Width               int         `json:"width,omitempty"`
	Height              int         `json:"height,omitempty"`
	Thumb               string      `json:"thumb,omitempty"`
	Caption             string      `json:"caption,omitempty"`
	ParseMode           string      `json:"parse_mode,omitempty"`
	DisableNotification bool        `json:"disable_notification,omitempty"`
	ReplyToMessageId    int         `json:"reply_to_message_id,omitempty"`
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`
}

type sendVoiceMethod struct {
	ChatId              int         `json:"chat_id"`
	Voice               string      `json:"voice"`
	Caption             string      `json:"caption,omitempty"`
	ParseMode           string      `json:"parse_mode,omitempty"`
	Duration            int         `json:"duration,omitempty"`
	DisableNotification bool        `json:"disable_notification,omitempty"`
	ReplyToMessageId    int         `json:"reply_to_message_id,omitempty"`
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`
}
