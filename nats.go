package main

import "github.com/pkg/errors"

func (b *Bot) subscribes() {
	_, err := b.NATSConn.Subscribe("send_msg", func(r *sendMessageMethod) {
		_, err := b.BotQueryBuilder.sendMessage(r)
		chkErr(errors.WithStack(err))
	})
	chkErr(errors.WithStack(err))

	_, err = b.NATSConn.Subscribe("forward_msg", func(r *forwardMessageMethod) {
		_, err := b.BotQueryBuilder.forwardMessage(r)
		chkErr(errors.WithStack(err))
	})
	chkErr(errors.WithStack(err))

	_, err = b.NATSConn.Subscribe("send_photo", func(r *sendPhotoMethod) {
		_, err := b.BotQueryBuilder.sendPhotoMethod(r)
		chkErr(errors.WithStack(err))
	})
	chkErr(errors.WithStack(err))

	_, err = b.NATSConn.Subscribe("send_audio", func(r *sendAudioMethod) {
		_, err := b.BotQueryBuilder.sendAudioMethod(r)
		chkErr(errors.WithStack(err))
	})
	chkErr(errors.WithStack(err))

	_, err = b.NATSConn.Subscribe("send_document", func(r *sendDocumentMethod) {
		_, err := b.BotQueryBuilder.sendDocumentMethod(r)
		chkErr(errors.WithStack(err))
	})
	chkErr(errors.WithStack(err))

	_, err = b.NATSConn.Subscribe("send_video", func(r *sendVideoMethod) {
		_, err := b.BotQueryBuilder.sendVideoMethod(r)
		chkErr(errors.WithStack(err))
	})
	chkErr(errors.WithStack(err))

	_, err = b.NATSConn.Subscribe("send_animation", func(r *sendAnimationMethod) {
		_, err := b.BotQueryBuilder.sendAnimationMethod(r)
		chkErr(errors.WithStack(err))
	})
	chkErr(errors.WithStack(err))

	_, err = b.NATSConn.Subscribe("send_voice", func(r *sendVoiceMethod) {
		_, err := b.BotQueryBuilder.sendVoiceMethod(r)
		chkErr(errors.WithStack(err))
	})
	chkErr(errors.WithStack(err))
}

func (method *sendMessageMethod) Send(r *Runtime) {
	chkErr(r.Bot.NATSConn.Publish("send_msg", method))
}

func (method *forwardMessageMethod) Send(r *Runtime) {
	chkErr(r.Bot.NATSConn.Publish("forward_msg", method))
}

func (method *sendPhotoMethod) Send(r *Runtime) {
	chkErr(r.Bot.NATSConn.Publish("send_photo", method))
}

func (method *sendAudioMethod) Send(r *Runtime) {
	chkErr(r.Bot.NATSConn.Publish("send_audio", method))
}

func (method *sendDocumentMethod) Send(r *Runtime) {
	chkErr(r.Bot.NATSConn.Publish("send_document", method))
}

func (method *sendVideoMethod) Send(r *Runtime) {
	chkErr(r.Bot.NATSConn.Publish("send_video", method))
}

func (method *sendAnimationMethod) Send(r *Runtime) {
	chkErr(r.Bot.NATSConn.Publish("send_animation", method))
}

func (method *sendVoiceMethod) Send(r *Runtime) {
	chkErr(r.Bot.NATSConn.Publish("send_voice", method))
}
