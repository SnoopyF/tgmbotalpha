package main

import (
	"fmt"
)

type Runtime struct {
	Results *ResultStruct
	Bot
}

type Method interface {
	Send(runtime *Runtime)
}

func (r *Runtime) Send(method Method) {
	method.Send(r)
}

func (r *Runtime) run() {
	for _, entities := range r.Results.Message.Entities {
		switch entities.Type {
		case "bot_command":
			switch r.Results.Message.Text[entities.Offset : entities.Offset+entities.Length] {
			case "/off":
				r.Bot.Exit <- true
				fmt.Println("Shutdown")
			case "/test_msg":
				r.Send(&sendMessageMethod{
					ChatId:           r.Results.Message.Chat.ID,
					Text:             "tested",
					ReplyToMessageId: r.Results.Message.MessageID,
				})
			case "/test":
				r.Send(&sendMessageMethod{
					ChatId:           r.Results.Message.Chat.ID,
					Text:             r.Results.Message.Text,
					ReplyToMessageId: r.Results.Message.MessageID,
					ReplyMarkup: InlineKeyboardMarkup{
						InlineKeyboard: [][]InlineKeyboardButton{{
							{Text: "12312", Url: "http://www.google.com/", Pay: true},
							{Text: "12312", Url: "http://www.google.com/", Pay: true},
						}},
					},
				})

				r.Send(&sendMessageMethod{
					ChatId:           r.Results.Message.Chat.ID,
					Text:             r.Results.Message.Text,
					ReplyToMessageId: r.Results.Message.MessageID,
					ReplyMarkup: ReplyKeyboardMarkup{
						Keyboard:        [][]KeyboardButton{{{Text: "123"}, {Text: "123"}, {Text: "123"}}},
						ResizeKeyboard:  true,
						OneTimeKeyboard: true,
					},
				})

				r.Send(&sendMessageMethod{
					ChatId:           r.Results.Message.Chat.ID,
					Text:             r.Results.Message.Text,
					ReplyToMessageId: r.Results.Message.MessageID,
					ReplyMarkup: ReplyKeyboardRemove{
						RemoveKeyboard: true,
						Selective:      true,
					},
				})

				r.Send(&sendMessageMethod{
					ChatId:           r.Results.Message.Chat.ID,
					Text:             r.Results.Message.Text,
					ReplyToMessageId: r.Results.Message.MessageID,
					ReplyMarkup: ForceReply{
						ForceReply: true,
						Selective:  true,
					},
				})

				r.Send(&sendMessageMethod{
					ChatId:    r.Results.Message.Chat.ID,
					Text:      "*bold text*",
					ParseMode: "Markdown",
				})

				r.Send(&sendMessageMethod{
					ChatId:    r.Results.Message.Chat.ID,
					Text:      "_italic text_",
					ParseMode: "Markdown",
				})

				r.Send(&sendMessageMethod{
					ChatId:    r.Results.Message.Chat.ID,
					Text:      "[inline URL](http://www.example.com/)",
					ParseMode: "Markdown",
				})

				r.Send(&sendMessageMethod{
					ChatId:    r.Results.Message.Chat.ID,
					Text:      "[inline mention of a user](tg://user?id=123456789)",
					ParseMode: "Markdown",
				})

				r.Send(&sendMessageMethod{
					ChatId:    r.Results.Message.Chat.ID,
					Text:      "`inline fixed-width code`",
					ParseMode: "Markdown",
				})

				r.Send(&sendMessageMethod{
					ChatId:    r.Results.Message.Chat.ID,
					Text:      "```block_language pre-formatted fixed-width code block```",
					ParseMode: "Markdown",
				})

				r.Send(&forwardMessageMethod{
					ChatId:              r.Results.Message.Chat.ID,
					FromChatId:          r.Results.Message.Chat.ID,
					DisableNotification: false,
					MessageId:           4298,
				})

				r.Send(&sendPhotoMethod{
					ChatId: r.Results.Message.Chat.ID,
					Photo:  "https://mholt.githur.Bot.io/json-to-go/resources/images/json-to-go.png",
				})

				r.Send(&sendPhotoMethod{
					ChatId: r.Results.Message.Chat.ID,
					Photo:  "AgADAgADPKwxG87hmUiriJT9NiWscdHiuQ8ABAEAAwIAA3kAA4gLAwABFgQ",
				})

				r.Send(&sendAudioMethod{
					ChatId: r.Results.Message.Chat.ID,
					Audio:  "https://s1.stopmusic.net/files/mp3/atl_-_serpantin_muzter.net_128.mp3",
				})

				r.Send(&sendDocumentMethod{
					ChatId:   r.Results.Message.Chat.ID,
					Document: "http://www.africau.edu/images/default/sample.pdf",
				})

				r.Send(&sendVideoMethod{
					ChatId: r.Results.Message.Chat.ID,
					Video:  "http://techslides.com/demos/sample-videos/small.mp4",
				})

				r.Send(&sendAnimationMethod{
					ChatId:    r.Results.Message.Chat.ID,
					Animation: "https://media.giphy.com/media/BfbUe877N4xsUhpcPc/giphy.gif",
				})

				r.Send(&sendVoiceMethod{
					ChatId: r.Results.Message.Chat.ID,
					Voice:  "AwADAgADnwQAAkjgoUj7fYy7r-0knhYE",
				})
			}
		}
	}
}
