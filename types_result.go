package main

import "sync"

type getMeStruct struct {
	Ok     bool `json:"ok"`
	Result struct {
		FirstName string `json:"first_name"`
		ID        uint   `json:"id"`
		IsBot     bool   `json:"is_bot"`
		Username  string `json:"username"`
	} `json:"result"`
}
type Results struct {
	Ok     bool           `json:"ok"`
	Result []ResultStruct `json:"result"`
}

type Result struct {
	Ok     bool         `json:"ok"`
	Result ResultStruct `json:"result"`
}

type ResultStruct struct {
	mux          sync.RWMutex `json:"-"`
	Processed    bool         `json:"-"`
	Synchronized bool         `bson:"-"`
	UpdateID     int          `bson:"_id" json:"update_id"`
	Message      Message      `json:"message"`
}
type Message struct {
	MessageID int `json:"message_id"`
	From      struct {
		ID           int    `json:"id"`
		IsBot        bool   `json:"is_bot"`
		FirstName    string `json:"first_name"`
		LastName     string `json:"last_name"`
		Username     string `json:"username"`
		LanguageCode string `json:"language_code"`
	} `json:"from"`
	Chat struct {
		ID        int    `json:"id"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Username  string `json:"username"`
		Type      string `json:"type"`
	} `json:"chat"`
	Date     int `json:"date"`
	Document struct {
		FileName string `json:"file_name"`
		MimeType string `json:"mime_type"`
		FileID   string `json:"file_id"`
		FileSize int    `json:"file_size"`
		Thumb    struct {
			FileID   string `json:"file_id"`
			FileSize int    `json:"file_size"`
			Width    int    `json:"width"`
			Height   int    `json:"height"`
		} `json:"thumb"`
	} `json:"document"`
	Photo []struct {
		FileID   string `json:"file_id"`
		FileSize int    `json:"file_size"`
		Height   int    `json:"height"`
		Width    int    `json:"width"`
	} `json:"photo"`
	Animation struct {
		FileName string `json:"file_name"`
		MimeType string `json:"mime_type"`
		Duration int    `json:"duration"`
		Width    int    `json:"width"`
		Height   int    `json:"height"`
		Thumb    struct {
			FileID   string `json:"file_id"`
			FileSize int    `json:"file_size"`
			Width    int    `json:"width"`
			Height   int    `json:"height"`
		} `json:"thumb"`
		FileID   string `json:"file_id"`
		FileSize int    `json:"file_size"`
	} `json:"animation"`
	Audio struct {
		Duration int    `json:"duration"`
		FileID   string `json:"file_id"`
		FileSize int    `json:"file_size"`
		MimeType string `json:"mime_type"`
	} `json:"audio"`
	Video struct {
		Duration int    `json:"duration"`
		Width    int    `json:"width"`
		Height   int    `json:"height"`
		MimeType string `json:"mime_type"`
		Thumb    struct {
			FileID   string `json:"file_id"`
			FileSize int    `json:"file_size"`
			Width    int    `json:"width"`
			Height   int    `json:"height"`
		} `json:"thumb"`
		FileID   string `json:"file_id"`
		FileSize int    `json:"file_size"`
	} `json:"video"`
	Voice struct {
		Duration int    `json:"duration"`
		MimeType string `json:"mime_type"`
		FileID   string `json:"file_id"`
		FileSize int    `json:"file_size"`
	} `json:"voice"`
	ForwardFrom struct {
		ID           int    `json:"id"`
		IsBot        bool   `json:"is_bot"`
		FirstName    string `json:"first_name"`
		LastName     string `json:"last_name"`
		Username     string `json:"username"`
		LanguageCode string `json:"language_code"`
	} `json:"forward_from"`
	ForwardDate int    `json:"forward_date"`
	Text        string `json:"text"`
	Entities    []struct {
		Offset int    `json:"offset"`
		Length int    `json:"length"`
		Type   string `json:"type"`
	} `json:"entities"`
}
