FROM golang:1.13-alpine3.10 as build
WORKDIR /go/src/tgm-bot
COPY . .
RUN CGO_ENABLED=0 go build --mod=vendor -o tgm-bot

FROM alpine:3.10
RUN apk update && apk add --no-cache \
        libc6-compat \
        ca-certificates

COPY --from=build /go/src/tgm-bot/tgm-bot /usr/bin/tgm-bot
CMD ["/usr/bin/tgm-bot"]
