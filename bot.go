package main

import (
	"context"
	"fmt"
	"github.com/nats-io/go-nats"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/url"
	"sync"
	"time"
)

type Bot struct {
	Id              uint
	Token           string
	FirstName       string
	Username        string
	Prefix          string
	BotQueryBuilder BotQueryBuilder
	Exit            chan bool
	NATSConn        *nats.EncodedConn
	UpdateOffset    int
	UpdatesStore    *UpdateStore
	MongoClient     *mongo.Client
}

type UpdateStore struct {
	mux   sync.RWMutex
	Store map[int]*ResultStruct
}

func (uS *UpdateStore) add(update *ResultStruct) {
	if uS.isUnique(update) {
		uS.mux.Lock()
		uS.Store[update.UpdateID] = update
		uS.mux.Unlock()
	}
}

func (uS *UpdateStore) delete(updateId int) {
	uS.mux.Lock()
	delete(uS.Store, updateId)
	uS.mux.Unlock()
}

func (uS *UpdateStore) getProcessedItems() [] *ResultStruct {
	var out []*ResultStruct
	for _, update := range uS.Store {
		if update.Synchronized == false && update.Processed == true {
			out = append(out, update)
		}
	}
	return out
}

func (uS *UpdateStore) isUnique(update *ResultStruct) bool {
	uS.mux.RLock()
	defer uS.mux.RUnlock()
	if uS.Store[update.UpdateID] == nil {
		return true
	}
	return false
}
func (b *Bot) listStore() {
	for {
		for _, u := range b.UpdatesStore.getProcessedItems() {
			collection := b.MongoClient.Database("tgm-bot").Collection("updates_store")
			insertResult, err := collection.InsertOne(context.TODO(), u)

			if err != nil {
				log.Println(err)
			} else {
				u.Synchronized = true
				fmt.Println("Synchronized update: ", insertResult.InsertedID)
			}
		}
		time.Sleep(10 * time.Second)
	}
}

func (b *Bot) updateChecker() {
	for {
		b.UpdatesStore.mux.Lock()
		for _, update := range b.UpdatesStore.Store {
			if update.Processed == false {
				err := b.NATSConn.Publish("updates", update)
				if err == nil {
					update.Processed = true
					totalIncomingMessages.Inc()
				}
			}
		}
		b.UpdatesStore.mux.Unlock()
		time.Sleep(500 * time.Millisecond)
	}
}

func botInit() (*Bot, error) {
	token := getEnv("TGM_TOKEN").toString()

	baseURL, err := url.Parse(fmt.Sprintf("https://api.telegram.org/bot%s/", token))
	chkErr(errors.WithStack(err))

	bot := Bot{
		BotQueryBuilder: BotQueryBuilder{BaseUrl: baseURL},
		Exit:            make(chan bool),
	}

	conn, err := nats.Connect("localhost")
	chkErr(errors.WithStack(err))

	bot.NATSConn, err = nats.NewEncodedConn(conn, nats.JSON_ENCODER)
	chkErr(errors.WithStack(err))

	bot.init(token)
	fmt.Printf("Bot init: %v\n", bot.Id)

	if err := bot.initDB(); err != nil {
		return nil, err
	}

	go bot.getUpdates()
	go bot.subscribes()
	go bot.updateChecker()
	go bot.listStore()

	return &bot, nil
}

func (b *Bot) getUpdates() {
	for {
		updates, err := b.BotQueryBuilder.getUpdates(b.UpdateOffset)
		if err != nil {
			return
		}
		for _, result := range updates.Result {
			b.UpdatesStore.add(&result)
		}
		b.setOffset()
		time.Sleep(250 * time.Millisecond)
	}
}

func (b *Bot) init(token string) {
	me, err := b.BotQueryBuilder.getMe()
	if err != nil {
		return
	}

	b.setPrefix("[ bot ]")
	b.setToken(token)
	b.setId(me.Result.ID)
	b.setFirstName(me.Result.FirstName)
	b.setUsername(me.Result.Username)
	b.initUpdateStore()
}

func (b *Bot) setPrefix(s string) {
	b.Prefix = s
}

func (b *Bot) setToken(s string) {
	b.Token = s
}

func (b *Bot) setId(u uint) {
	b.Id = u
}

func (b *Bot) setFirstName(s string) {
	b.FirstName = s
}

func (b *Bot) setUsername(s string) {
	b.Username = s
}

func (b *Bot) initUpdateStore() {
	b.UpdatesStore = &UpdateStore{Store: make(map[int]*ResultStruct)}
}

func (b *Bot) setOffset() {
	b.UpdateOffset = b.getMaxIdFromDBase()
}

func (b *Bot) getMaxIdFromDBase() int {
	var update *ResultStruct

	opt := options.FindOne().SetSort(bson.D{{"_id", -1}})
	err := b.MongoClient.Database("tgm-bot").Collection("updates_store").FindOne(context.Background(), bson.D{}, opt).Decode(&update)
	if err != nil {
		return 0
	} else {
		return update.UpdateID
	}
}
