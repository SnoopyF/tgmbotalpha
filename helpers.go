package main

import (
	"github.com/pkg/errors"
	"log"
	"os"
	"strconv"
)

type Env struct {
	raw string
}

func getEnv(name string) *Env {
	e := Env{}
	e.raw = os.Getenv(name)
	return &e
}

func (e *Env) toInt() int {
	i, err := strconv.Atoi(e.raw)
	chkErr(errors.WithStack(err))
	return i
}

func (e *Env) toString() string {
	return e.raw
}

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func chkErr(err error) {
	if err, ok := err.(stackTracer); ok {
		for _, f := range err.StackTrace() {
			log.Printf("%+s:%d\n", f, f)
		}
	}

	if err != nil {
		log.Println(err)
	}
}
