package main

type InlineKeyboardMarkup struct {
	InlineKeyboard [][]InlineKeyboardButton `json:"inline_keyboard,omitempty"`
}

type InlineKeyboardButton struct {
	Text              string       `json:"text,omitempty"`
	Url               string       `json:"url,omitempty"`
	LoginUrl          LoginUrl     `json:"login_url,omitempty"`
	CallbackData      string       `json:"callback_data,omitempty"`
	SwitchInlineQuery string       `json:"switch_inline_query,omitempty"`
	CallbackGame      CallbackGame `json:"callback_game,omitempty"`
	Pay               bool         `json:"pay,omitempty"`
}

type ReplyKeyboardMarkup struct {
	Keyboard        [][]KeyboardButton `json:"keyboard,omitempty"`
	ResizeKeyboard  bool               `json:"resize_keyboard,omitempty"`
	OneTimeKeyboard bool               `json:"one_time_keyboard,omitempty"`
	Selective       bool               `json:"selective,omitempty"`
}

type ReplyKeyboardRemove struct {
	RemoveKeyboard bool `json:"remove_keyboard"`
	Selective      bool `json:"selective"`
}

type ForceReply struct {
	ForceReply bool `json:"force_reply"`
	Selective  bool `json:"selective"`
}

type KeyboardButton struct {
	Text            string `json:"text,omitempty"`
	RequestContact  bool   `json:"request_contact,omitempty"`
	RequestLocation bool   `json:"request_location,omitempty"`
}

type LoginUrl struct {
	Url                string `json:"url,omitempty"`
	ForwardText        string `json:"forward_text,omitempty"`
	BotUsername        string `json:"bot_username,omitempty"`
	RequestWriteAccess bool   `json:"request_write_access,omitempty"`
}

type CallbackGame struct {
	UserId             int    `json:"user_id,omitempty"`
	Score              int    `json:"score,omitempty"`
	Force              bool   `json:"force,omitempty"`
	DisableEditMessage bool   `json:"disable_edit_message,omitempty"`
	ChatId             int    `json:"chat_id,omitempty"`
	MessageId          int    `json:"message_id,omitempty"`
	InlineMessageId    string `json:"inline_message_id,omitempty"`
}
