package main

import "github.com/prometheus/client_golang/prometheus"

var (
	totalIncomingMessages = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "tgm_bot_message_total",
		Help: "number of incoming messages",
	})
)

func init() {
	prometheus.MustRegister(totalIncomingMessages)
}
