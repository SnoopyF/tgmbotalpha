package main

import (
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net/http"
)

func main() {
	go func() {
		http.Handle("/metrics", promhttp.Handler())

		err := http.ListenAndServe("localhost:9090", nil)
		if err != nil {
			log.Printf("http.ListenAndServer: %v\n", err)
		}
	}()

	b, err := botInit()
	if err != nil {
		chkErr(errors.WithStack(err))
		return
	}

	_, err = b.NATSConn.Subscribe("updates", func(r *ResultStruct) {
		runtime := Runtime{
			Results: r,
			Bot:     *b,
		}
		go runtime.run()
	})

	if err != nil {
		chkErr(errors.WithStack(err))
		b.Exit <- true
	}

	<-b.Exit
}
