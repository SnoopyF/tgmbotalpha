package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"net/url"
)

type BotQueryBuilder struct {
	BaseUrl *url.URL
}

func methodProcessor(b *BotQueryBuilder, method string, arg interface{}) (interface{}, error) {
	var byteData []byte
	var err error

	updateUrl := *b.BaseUrl
	updateUrl.Path = fmt.Sprintf("%s%s", b.BaseUrl.Path, method)

	if arg != nil {
		byteData, err = json.Marshal(arg)
		chkErr(errors.WithStack(err))
	}

	switch method {
	case "getMe":
		var out getMeStruct

		body, err := sendPost(&updateUrl, nil)
		if err != nil {
			return nil, err
		}
		if err := json.Unmarshal(body, &out); err != nil {
			return nil, err
		}
		return &out, nil
	case "getUpdates":
		var out Results
		body, err := sendPost(&updateUrl, bytes.NewReader(byteData))
		if err != nil {
			return nil, err
		}
		if err := json.Unmarshal(body, &out); err != nil {
			return nil, err
		}
		return &out, nil
	default:
		var out Result
		body, err := sendPost(&updateUrl, bytes.NewReader(byteData))
		if err != nil {
			return nil, err
		}
		if err := json.Unmarshal(body, &out); err != nil {
			return nil, err
		}
		return &out, nil
	}
}

func (b *BotQueryBuilder) getMe() (*getMeStruct, error) {
	out, err := methodProcessor(b, "getMe", nil)
	if err != nil {
		return nil, err
	}
	return out.(*getMeStruct), nil
}

func (b *BotQueryBuilder) getUpdates(offset int) (*Results, error) {
	out, err := methodProcessor(b, "getUpdates", getUpdatesMethod{Offset: offset})
	if err != nil {
		return nil, err
	}
	return out.(*Results), nil
}

func (b *BotQueryBuilder) sendMessage(msg *sendMessageMethod) (*Result, error) {
	out, err := methodProcessor(b, "sendMessage", *msg)
	if err != nil {
		return nil, err
	}
	return out.(*Result), nil
}

func (b *BotQueryBuilder) forwardMessage(msg *forwardMessageMethod) (*Result, error) {
	out, err := methodProcessor(b, "forwardMessage", *msg)
	if err != nil {
		return nil, err
	}
	return out.(*Result), nil
}

func (b *BotQueryBuilder) sendPhotoMethod(msg *sendPhotoMethod) (*Result, error) {
	out, err := methodProcessor(b, "sendPhoto", *msg)
	if err != nil {
		return nil, err
	}
	return out.(*Result), nil
}

func (b *BotQueryBuilder) sendAudioMethod(msg *sendAudioMethod) (*Result, error) {
	out, err := methodProcessor(b, "sendAudio", *msg)
	if err != nil {
		return nil, err
	}
	return out.(*Result), nil
}

func (b *BotQueryBuilder) sendDocumentMethod(msg *sendDocumentMethod) (*Result, error) {
	out, err := methodProcessor(b, "sendDocument", *msg)
	if err != nil {
		return nil, err
	}
	return out.(*Result), nil
}

func (b *BotQueryBuilder) sendVideoMethod(msg *sendVideoMethod) (*Result, error) {
	out, err := methodProcessor(b, "sendVideo", *msg)
	if err != nil {
		return nil, err
	}
	return out.(*Result), nil
}

func (b *BotQueryBuilder) sendAnimationMethod(msg *sendAnimationMethod) (*Result, error) {
	out, err := methodProcessor(b, "sendAnimation", *msg)
	if err != nil {
		return nil, err
	}
	return out.(*Result), nil
}

func (b *BotQueryBuilder) sendVoiceMethod(msg *sendVoiceMethod) (*Result, error) {
	out, err := methodProcessor(b, "sendVoice", *msg)
	if err != nil {
		return nil, err
	}
	return out.(*Result), nil
}
